package br.edu.cest.pet.entity;

import java.math.BigDecimal;

public class Animal {
	private String nome;
	private String especie;
	private String locomo;
	private BigDecimal peso;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEspecie() {
		return especie;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	public String getLocomo() {
		return locomo;
	}
	public void setLocomo(String locomo) {
		this.locomo = locomo;
	}
	public BigDecimal getPeso() {
		return peso;
	}
	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}
	

}
